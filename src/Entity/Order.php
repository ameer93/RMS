<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=0)
     */
    private $total;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="decimal", precision=0)
     */
    private $cash_in;

    /**
     * @ORM\Column(type="decimal", precision=0)
     */
    private $payment;

    /**
     * @ORM\Column(type="decimal", precision=0)
     */
    private $change_;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    public function getId()
    {
        return $this->id;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCashIn()
    {
        return $this->cash_in;
    }

    public function setCashIn($cash_in): self
    {
        $this->cash_in = $cash_in;

        return $this;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function setPayment($payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getChange()
    {
        return $this->change_;
    }

    public function setChange($change_): self
    {
        $this->change_ = $change_;

        return $this;
    }
}
